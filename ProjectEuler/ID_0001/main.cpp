#include <iostream>

#define UPPER_LIMIT 1000

int main(int argc, char** argv){
  std::cout << "Sum of all multiples of 3 or 5 between 0 and " << UPPER_LIMIT;
  std::cout << ": " << std::endl;

  unsigned sum = 0;
  for(unsigned i = 0; i < UPPER_LIMIT; ++i){
    if(i % 3 == 0 || i % 5 == 0)
      sum += i;
  }

  std::cout << "sum: " << sum << std::endl;
  return 0;
}
