# Project Euler

This folder contains all the programs I wrote by myself to solve problems on [Project Euler](https://projecteuler.net/).

Disclaimer: Although I share the programs to solve the problems, I don't explicitly share the expected solutions for
each problem. This is because it's against Project Euler rules. That being said, anyone can reproduce the given code
and compute the solution. Be smarter than this and just use this as a base to compare with your own solutions. Most
of my programs are not optimized and use brute-force approaches.

![total solutions](https://projecteuler.net/profile/boreec.png)