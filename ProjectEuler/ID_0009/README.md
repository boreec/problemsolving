# Special Pythagorean triplet

## Problem 9

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a² + b² = c²

For example, 3² + 4² = 5²

There exists exactly one Pythagorean triplet for which a + b + c = 1000.

Find the product *abc*.

## Compilation and running

In the same directory as this file:

```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./ID_0009
```