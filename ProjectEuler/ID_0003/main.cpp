#include <cmath>
#include <iostream>

bool isPrime(unsigned long long n){
  if(n % 2 == 0)
    return false;
  unsigned long long i = 3;
  while(i < std::sqrt(n) && n % i != 0){
    i += 2;
  }
  return n % i != 0;
}

int main(int argc, char** argv){
  constexpr unsigned long long target = 600'851'475'143;
  constexpr unsigned long long target_sqrt = std::sqrt(target); 

  int i = target_sqrt;
  while(i > 2 && !(target % i == 0 && isPrime(i))){
    --i;
  }

  if(i > 2)
    std::cout << "biggest prime factor of " <<  target << " is " << i << "." << std::endl;
  else
    std::cout << "No prime factor found for " << target << "." << std::endl;
  return 0;
}
