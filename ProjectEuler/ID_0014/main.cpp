#include <iostream>

unsigned collatzSequenceLength(unsigned i){
  unsigned length = 1;
  while(i != 1){
    if(i % 2 == 0){
      i /= 2;
    }else{
      i = 3 * i + 1;
    }
    length++;
  }
  return length;
}

int main(int argc, char** argv){
  unsigned longestStartingPoint = 1;
  unsigned longestSequence = 1;

  for(unsigned i = 2; i < 1'000'000; ++i){
    unsigned tmp = collatzSequenceLength(i);
    if(tmp > longestSequence){
      longestSequence = tmp;
      longestStartingPoint = i;
      std::cout << "new longest sequence:" << longestSequence;
      std::cout << " from " << longestStartingPoint << std::endl;
    }
  }
  return 0;
}
