#include <iostream>
#include <numeric>
#include <vector>
#include <cmath>

bool isPrime(unsigned long long a){
  if(a % 2 == 0)
    return false;
  unsigned long long i = 3;
  while(i < sqrt(a) && a % i != 0){
    i += 2;
  }

  return !(i <= sqrt(a));
}

int main(int argc, char** argv){
  const unsigned UPPER_LIMIT = 2'000'000;

  unsigned long long sum = 2 + 3;
  unsigned long long k = 1;
  while(6 * k + 1 < UPPER_LIMIT){
    if(isPrime(6 * k - 1)){
      sum += 6 * k - 1;
    }
    if(isPrime(6 * k + 1)){
      sum += 6 * k + 1;
    }
    k++;
  }
  std::cout << "sum of primes below " << UPPER_LIMIT << " is " << sum << std::endl;
  return 0;
}
