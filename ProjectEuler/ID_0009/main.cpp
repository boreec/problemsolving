#include <cmath>
#include <iostream>

int main(int argc, char** argv){
  unsigned target = 1000;
  unsigned product = 0;
  for(int a = 1; a < target; ++a){
    for(int b = 1; b < target; ++b){
      unsigned c = a * a + b * b;
      if(a + b + sqrt(c) == target){
	product = a * b * sqrt(c);
      }
    }
  }

  std::cout << "product of abc is " << product << std::endl;
  return 0;
}
