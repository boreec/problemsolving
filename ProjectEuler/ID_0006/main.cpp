#include <iostream>
#include <cmath>

unsigned squareOfSums(unsigned n){
  return std::pow(n * (n + 1) / 2, 2);
}

unsigned sumOfSquares(unsigned n){
  return n * (n + 1) * (2 * n + 1) / 6;
}

int main(int argc, char** argv){
  std::cout << "Difference between sum of squares and square of sums is:";
  std::cout << std::endl << squareOfSums(100) - sumOfSquares(100) << std::endl;
  return 0;
}
