#include <cmath>
#include <iostream>

// return the n-th triangular number
unsigned long long triangularNumber(unsigned long long n){
  return n * (n + 1) / 2;
}

// return the number of factor for n
unsigned long long factorCount(unsigned long long n){
  unsigned long long factor = 0;
  for(unsigned long long i = 1; i <= sqrt(n); ++i){
    if(n % i == 0){
      if(n / i == i)
	factor++;
      else
	factor += 2;
    }
  }
  return factor;
}

int main(int argc, char** argv){
  unsigned long long maxFactor = 0;
  unsigned long long factorTarget = 500;
  unsigned long long i = 0;
  while(maxFactor < factorTarget){
    i++;
    unsigned long long tNumber = triangularNumber(i);
    unsigned long long tFactor = factorCount(tNumber);
    if(tFactor > maxFactor){
      maxFactor = tFactor;
      std::cout << tNumber << " has " << maxFactor << " factors." << std::endl;
    }
  }
  return 0;
}
