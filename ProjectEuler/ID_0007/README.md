# 10001st prime

## Problem 7

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?

## Compilation and running

In the same directory as this file:

```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./ID_0007
```