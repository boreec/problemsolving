#include <iostream>
#include <string>

// Return the result between a + b, where a and b
// are both strings representing unsigned numbers.
std::string add(std::string a, std::string b){
  std::string result = "";
  std::string shorterNumber = (a.size() <= b.size()) ? a : b;
  std::string longerNumber = (a.size() > b.size()) ? a : b;

  unsigned long long lengthDifference = longerNumber.size() - shorterNumber.size();
  if(lengthDifference){
    std::string unsignificantZeros(lengthDifference, '0');
    shorterNumber = unsignificantZeros + shorterNumber;
  }
  
  

  unsigned long long remainder = 0;
  for(int long long i = shorterNumber.size() - 1; i >= 0; --i){
    remainder += ((int)shorterNumber[i] - (int)'0') + ((int)longerNumber[i] - (int)'0');
    result.insert(0, std::to_string(remainder % 10));
    remainder /= 10;
  }
  if(remainder)
    result.insert(0, std::to_string(remainder));
  return result;
}

// Return the sum of every digits found in a string.
unsigned long long sumOfDigits(std::string a){
  unsigned long long sum = 0;
  for(const auto& c : a){
    if(c >= '0' && c <= '9'){
      sum += c - '0';
    }
 
  }
  return sum;
}

int main(int argc, char** argv){

  std::string sum = "2";
  for(int i = 2; i <= 1000; ++i){
    sum = add(sum, sum);
    std::cout << i  << ":2^" << i << " sum of digits:" << sumOfDigits(sum) << std::endl;
  }
  return 0;
}
