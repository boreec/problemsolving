#include <iostream>
#include <cmath>

bool isPrime(unsigned a){
  if(a % 2 == 0)
    return false;
  unsigned i = 3;
  while(i <= std::sqrt(a) && a % i != 0){
    i += 2;
  }

  return !(i <= std::sqrt(a));
}

int main(int argc, char** argv){
  unsigned lastPrime = 3;
  unsigned primeCount = 2; // 2 and 3
  unsigned k = 1;
  while(primeCount != 10001){
    if(isPrime(6 * k - 1)){
      primeCount++;
      lastPrime = 6 * k - 1;
    }
    if(isPrime(6 * k + 1) && primeCount != 10001){
      primeCount++;
      lastPrime = 6 * k + 1;
    }
    k++;
  }
  std::cout << "10001st prime number is " << lastPrime << std::endl;
  return 0;
}
