#include <iostream>
#include <string>
#include <unordered_map>

// count the number of letters in a string.
// the letters are counted if between a to z.
unsigned letterCount(std::string a){
  unsigned result = 0;
  for(const auto& c : a){
    if(c >= 'a' && c <= 'z')
      result++;
  }
  return result;
}

int main(int argc, char** argv){
  std::unordered_map<unsigned, std::string> words;
  words[1] = "one";
  words[2] = "two";
  words[3] = "three";
  words[4] = "four";
  words[5] = "five";
  words[6] = "six";
  words[7] = "seven";
  words[8] = "eight";
  words[9] = "nine";
  words[10] = "ten";
  words[11] = "eleven";
  words[12] = "twelve";
  words[13] = "thirteen";
  words[14] = "fourteen";
  words[15] = "fifteen";
  words[16] = "sixteen";
  words[17] = "seventeen";
  words[18] = "eighteen";
  words[19] = "nineteen";
  words[20] = "twenty";
  words[30] = "thirty";
  words[40] = "forty";
  words[50] = "fifty";
  words[60] = "sixty";
  words[70] = "seventy";
  words[80] = "eighty";
  words[90] = "ninety";
  words[100] = "hundred";

  unsigned long long sum = 0;
  for(int i = 1; i <= 1000; ++i){
    std::string completeNumber = "";
    if(i < 100){
      if(i <= 20){
	completeNumber = words[i];
      }else{
	if(i % 10 == 0){
	  completeNumber = words[i % 100];
	}else{
	  completeNumber = words[(i % 100) - (i % 10)] + "-" + words[i % 10];
	}
      }
    }else if(i < 1000){
      unsigned hundreds = i / 100;
      completeNumber = words[hundreds] + " hundred";
      if(i % 100 > 0){
	completeNumber += " and ";
	if(i % 100 < 20){
	  completeNumber += words[i % 100];
	}else{
	  if(i % 10 == 0){
	    completeNumber += words[i % 100];
	  }else{
	    completeNumber += words[(i % 100) - (i % 10)] + "-" + words[i % 10];
	  }
	}
      }
    }else if(i == 1000){
      completeNumber = "one thousand";
    }
    sum += letterCount(completeNumber);
  }

  std::cout << "The number letter count between 1 and 1000 is " << sum << std::endl;
  return 0;
}
