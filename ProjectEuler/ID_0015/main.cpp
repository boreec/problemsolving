#include <iostream>


int main(int argc, char** argv){
  /**
     If we represent the way to go through the grid with the letters "S" for "going south" and
     "E" for "going west", then all the lattice paths corresponding to a 2x2 grids are :
      - "EESS", "ESES", "ESSE", "SEES", "SESE", "SSEE".
      In other words, the problem is to find all permutations for a string of size 4 with
      2 characters being "E" and 2 characters being "S".

      This is given by the formula :
       n! / |S|! * |E|! -> 4! / 2! * 2! = 6 paths.

      For a grid 20x20, we have to solve :
       40! / 20! * 20!
       
      The numbers does not fit into memory, but after simplifying the multiplication/division
      I found the result to be equivalent to 37 * 31 * 29 * 23 * 21 * 13 * 12 * 11 *5. 
   **/

  unsigned long long result =  1;
  result *= 5;
  result *= 11;
  result *= 12;
  result *= 13;
  result *= 21;
  result *= 23;
  result *= 29;
  result *= 31;
  result *= 37;
  std::cout << "20x20: " << result << std::endl;
  return 0;
}
