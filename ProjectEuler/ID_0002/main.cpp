#include <iostream>

#define UPPER_LIMIT 4'000'000

int main(int argc, char** argv){
  std::cout << "Sum of even-valued terms of Fibonacci sequences below " << UPPER_LIMIT;
  std::cout << ": " << std::endl;

  unsigned term_1 = 1;
  unsigned term_2 = 2;
  unsigned sum = 2;
  
  while(term_1 < UPPER_LIMIT){
    unsigned tmp = term_1 + term_2;
    if(tmp % 2 == 0)
      sum += tmp;
    term_1 = term_2;
    term_2 = tmp;
  }

  std::cout << "sum: " << sum << std::endl;
  return 0;
}
