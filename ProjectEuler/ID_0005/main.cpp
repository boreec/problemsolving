#include <algorithm>
#include <iostream>
#include <string>

bool isEvenlyDivisible(unsigned a){
  int i = 1;
  while(i < 20 && a % i == 0){
    ++i;
  }
  return !(i < 20);
}

int main(int argc, char** argv){

  int i = 1;
  while(!isEvenlyDivisible(i)){
    ++i;
  }
  std::cout << "Smallest positive number that is evenly divisible by all of the numbers from 1 to 20: ";
  std::cout << std::endl << i << std::endl;
  return 0;
}
