#include <algorithm>
#include <iostream>
#include <string>

bool isPalindromic(unsigned long long n){
  std::string s = std::to_string(n);
  std::string copy(s);
  std::reverse(s.begin(), s.end());
  return s == copy;
}

int main(int argc, char** argv){
 int biggestPalindromicNumber = 1;
 for(int i = 1; i < 1000; ++i){
   for(int j = 1; j < 1000; ++j){
     if(isPalindromic(i * j) && i * j > biggestPalindromicNumber){
       biggestPalindromicNumber = i * j;
     }
   }
 }

 
 if(biggestPalindromicNumber > 1){
   std::cout << "The biggest palindromic number made from the product of two 3-digit numbers is: ";
   std::cout << std::endl << biggestPalindromicNumber << std::endl;
 }
 return 0;
}
