# Lattice paths

## Problem 15

Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.

![example_image](https://projecteuler.net/project/images/p015.png)

How many such routes are there through a 20×20 grid?

## Compilation and running

In the same directory as this file:

```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./ID_0015
```
